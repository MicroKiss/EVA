#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>


    enum Players  { BLUE,RED};

class Game : public QWidget  //ez az ablak jeleníti meg a játékot
{
    Q_OBJECT

public:
    explicit Game(QWidget *parent = nullptr);
    ~Game(){}

private slots:
    void BtnClick();  // gamefield's btn
    void NewGameBtnClicked();  //NewGame btn
    void FieldSizeBtn();

private:
    ///Gombok
    QPushButton* NewGameBtn;
    QPushButton* GameSizeBtn;
    QVector<QVector<QPushButton*> > btnTable; //gamefield
    QGridLayout* tableLayout;
    QVBoxLayout* mainLayout;
    void generateTable(int n);
    void destroyTable();
    void stepGame(int x, int y);

    void NewGame();
    int ScoreBlue,ScoreRed;
    void isOver();

    Players CurrPlayer;
    int _CurrGameSize = 3;
    int _SelectedGameSize = 3;

};

#endif // GAME_H
