#include "game.h"
#include <QMessageBox>
#include <QString>
#include <iostream>
Game::Game(QWidget *parent) : QWidget(parent)
{
setMinimumSize(500,500);
setBaseSize(500,500);
NewGameBtn = new QPushButton("New Game");
NewGameBtn->setFont(QFont("Times New Roman",20,QFont::Bold));
connect(NewGameBtn,SIGNAL(clicked()),this,SLOT(NewGameBtnClicked()));

mainLayout = new QVBoxLayout(); //vertical position of the whole thing
GameSizeBtn = new QPushButton("Field Size: " + (QString::number(_CurrGameSize)) + " x "+ QString::number(_CurrGameSize));
GameSizeBtn->setFont(QFont("Times New Roman",15,QFont::Bold));
connect(GameSizeBtn,SIGNAL(clicked()),this,SLOT(FieldSizeBtn()));

QGridLayout *btnLayout =new QGridLayout();   //so they are next to each other
btnLayout->addWidget(NewGameBtn,0,0);
btnLayout->addWidget(GameSizeBtn,0,1);
mainLayout->addLayout(btnLayout);

tableLayout = new QGridLayout(); // to position the buttons in a # way

mainLayout->addLayout(tableLayout);
setLayout(mainLayout);
NewGame();
}


void Game::NewGame()
{

    generateTable(_SelectedGameSize);
    setMinimumSize(130*_CurrGameSize,130*_CurrGameSize);
    resize(130*_CurrGameSize,130*_CurrGameSize);

    for (int i = 0; i < _CurrGameSize; ++i)
        for (int j = 0; j < _CurrGameSize; ++j)
        {
            btnTable[i][j]->setText("0"); // set all values to 0
            btnTable[i][j]->setStyleSheet( "background-color: white"); // make it nice white
            btnTable[i][j]->setEnabled(true); // Turn them on :3
        }

    CurrPlayer = BLUE; // Blue moves first
    ScoreBlue = ScoreRed = 0;
}

void Game::generateTable(int n)
{
    if(_CurrGameSize)   // if > 0
     destroyTable();    //destroys the objects fro previous game
    _CurrGameSize = n;

    btnTable.resize(_CurrGameSize);
    for(int i = 0;i < btnTable.size();++i)
        {
             btnTable[i].resize(_CurrGameSize);
            for(int j = 0;j < btnTable[i].size();++j)       //this creates the new buttons and set their attrs
            {
                btnTable[i][j]= new QPushButton(this);
                btnTable[i][j]->setFont(QFont("Times New Roman",75,QFont::Bold));
                btnTable[i][j]->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
                tableLayout->addWidget(btnTable[i][j],i,j);  //place button to the grid

                connect(btnTable[i][j],SIGNAL(clicked()),this,SLOT(BtnClick()));
            }
        }
}

void Game::destroyTable()
{
    if(btnTable.size())  // if > 0
       {
            for (int i = 0; i < _CurrGameSize;++i)
            {
                for (int j = 0; j < _CurrGameSize;++j)
                {
                    delete btnTable[i][j];
                }
            }
       }
}

void increment_Btn(QPushButton *btn,Players player,int &scoreblue,int &scorered)
{
auto tmp = btn->text();
int value = tmp.split(" ")[0].toInt();

 if (value == 4)  ///So the buttons won't go over 4
 {
    return;
 }
 else
 {
     btn->setText(QString::number(++value));

     if (value == 4)
    {
        btn->setEnabled(false);   ///You won't be able to use the button anymore
        btn->setStyleSheet( player == RED ? "background-color: red" : "background-color: blue");
        player == RED ?  ++scorered : ++scoreblue;
    }
 }

}

int abs(int c){

    return c < 0 ? -c:c;
}

void Game::stepGame(int _y, int _x)
{

    increment_Btn(btnTable[_y][_x],CurrPlayer,ScoreBlue,ScoreRed);  //increase itself
    for(int x = _x-1;x<_x+2;++x)   ///and the buttons next to it
    {
        for(int y = _y-1;y < _y+2;++y)
        {
            if(y < _CurrGameSize&& y >= 0 && x <_CurrGameSize && x >=0  //the button is in the board
                    && abs(x-_x+y-_y) == 1)                             //and it's 1 far away
            increment_Btn(btnTable[y][x],CurrPlayer,ScoreBlue,ScoreRed);
        }
    }
    CurrPlayer = CurrPlayer == BLUE ? RED : BLUE;
    isOver();


}

void Game::BtnClick()
{
    QPushButton* senderButton = dynamic_cast<QPushButton*>(QObject::sender()); //so the next line works
    int location = tableLayout->indexOf(senderButton);
    stepGame(location/_CurrGameSize,location % _CurrGameSize);

}

void Game::NewGameBtnClicked()
{
    NewGame();
}

void Game::FieldSizeBtn()
{
    _SelectedGameSize =std::max((_SelectedGameSize+1) % 8,3) ;
    GameSizeBtn->setText("Field Size: " + (QString::number(_SelectedGameSize)) + " x "+ QString::number(_SelectedGameSize));
}

void Game::isOver()
{
if(ScoreBlue + ScoreRed != _CurrGameSize*_CurrGameSize)
    return;

if(ScoreBlue == ScoreRed)
{
    QMessageBox::information(this, ("Game End!"), ("NOBODY WINS, EVERYBODY LOSES"));
}
else
{
    QMessageBox::information(this, ("Game End!"), ( ScoreBlue > ScoreRed ? "Blue Won!": "Red Won!"));
}


NewGame();

}
