#include "display.h"

Displayy::Displayy(QWidget *parent)
    : QWidget(parent)
{
    setFixedSize(500,600);
    setWindowTitle("Sodoku");
    GameLogic  = new Logic();
        connect(GameLogic, SIGNAL(gameover()), this, SLOT(GameOver()));

    btn_NewGame = new QPushButton("New Game");
    btn_Pause = new QPushButton("Pause");

    lcd_Time = new QLCDNumber();
    connect(GameLogic, SIGNAL(time_change(int)), lcd_Time, SLOT(display(int)));

    {
    btn_NewGame->setFont(QFont("Times New Roman",20,QFont::Bold));
    lcd_Time->setFont(QFont("Times New Roman",20,QFont::Bold));
    btn_Pause->setFont(QFont("Times New Roman",20,QFont::Bold));

    connect(btn_NewGame, SIGNAL(clicked()), this, SLOT(startNewGame()));
    connect(btn_Pause, SIGNAL(clicked()), this, SLOT(pauseGame()));

    }
    QHBoxLayout *hLayout = new QHBoxLayout();
    hLayout->addWidget(btn_NewGame);
    hLayout->addWidget(lcd_Time);
    hLayout->addWidget(btn_Pause);


    // game buttons
    buttonGridLayout = new QGridLayout(); // to position the buttons in a # way

    btnTable.resize(9);
    for (int i =0; i < 9;++i)
    {
        btnTable[i].resize(9);
        for (int j = 0;j < 9;++j)
        {
            btnTable[i][j] = new QPushButton();
            btnTable[i][j]->setFont(QFont("Times New Roman",20,QFont::Bold));
            buttonGridLayout->addWidget(btnTable[i][j],i,j);
            connect(btnTable[i][j],SIGNAL(clicked()),this,SLOT(btnClick()));

            ///House coloring
            if ( GameLogic->GetHouse(i,j) & 1)
                 btnTable[i][j]->setStyleSheet("background:rgb(100,100,150);");
            else
                btnTable[i][j]->setStyleSheet("background:rgb(200,100,150);");
        }
    }

         //save and load btns
     btn_Save = new QPushButton("Save");
     btn_Load = new QPushButton("Load");
     connect(btn_Save, SIGNAL(clicked()), this, SLOT(saveGame()));
     connect(btn_Load, SIGNAL(clicked()), this, SLOT(loadGame()));
     btn_Save->setFont(QFont("Times New Roman",20,QFont::Bold));
     btn_Load->setFont(QFont("Times New Roman",20,QFont::Bold));

        //hurry button and lcd
     btn_Hardcore = new QPushButton("Hurry");
     btn_Hardcore->setFont(QFont("Times New Roman",20,QFont::Bold));
     connect(btn_Hardcore, SIGNAL(clicked()),this,SLOT(hardcoremode()));
     btn_Hardcore_interval = new QPushButton("Set time");
     btn_Hardcore_interval->setFont(QFont("Times New Roman",20,QFont::Bold));
     connect(btn_Hardcore_interval, SIGNAL(clicked()),this,SLOT(hardcoreinterval()));

     lcd_TimeHardcore = new QLCDNumber();

     lcd_TimeHardcore->setFont(QFont("Times New Roman",20,QFont::Bold));
     connect(GameLogic, SIGNAL(time_hardcore_change(int)), lcd_TimeHardcore, SLOT(display(int)));

     QHBoxLayout *hLayout3 = new QHBoxLayout();
     hLayout3->addWidget(btn_Hardcore);
     hLayout3->addWidget(lcd_TimeHardcore);
     hLayout3->addWidget(btn_Hardcore_interval);

    QHBoxLayout *hLayout2 = new QHBoxLayout();
    hLayout2->addWidget(btn_Save);
    hLayout2->addWidget(btn_Load);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(hLayout);
    mainLayout->addLayout(buttonGridLayout);
    mainLayout->addLayout(hLayout2);
    mainLayout->addLayout(hLayout3);

    setLayout(mainLayout);

}

Displayy::~Displayy()
{
    for (auto i : btnTable)
        for(auto j : i)
            delete j;
}


void Displayy::startNewGame()
{
GameLogic->startNewGame();

lcd_Time->display(GameLogic->_gametime);
lcd_TimeHardcore->display(GameLogic->hardcoretime);

for(int i = 0;i < 9;++i)
    for(int j = 0;j < 9;++j)
    {
        btnTable[i][j]->setText("");
        btnTable[i][j]->setDisabled(0);
        if ( GameLogic->GetHouse(i,j) & 1)
             btnTable[i][j]->setStyleSheet("background:rgb(100,100,150);");
        else
            btnTable[i][j]->setStyleSheet("background:rgb(200,100,150);");
    }
}

void Displayy::pauseGame()
{
    btn_Pause->setText(btn_Pause->text() =="Pause" ? "Continue" : "Pause");
    GameLogic->pauseGame();
}

void Displayy::saveGame()
{
    GameLogic->saveGame();

}

void Displayy::loadGame()
{
    GameLogic->loadGame();

lcd_Time->display(GameLogic->_gametime);

for(int i = 0;i < 9 ;++i)
{
    for (int j = 0;j < 9;++j)
    {
        int tmp = GameLogic->ValueMatrix[i][j];
        btnTable[i][j]->setText(tmp == 0 ? "" : QString::number(tmp));
         if (tmp != 0)
         {
             btnTable[i][j]->setDisabled(true);
             btnTable[i][j]->setStyleSheet("background:rgb(250,250,250)");

         }
    }
}

}

void Displayy::btnClick()
{
    if (GameLogic->paused)
        return;

    QPushButton* senderButton = dynamic_cast<QPushButton*>(QObject::sender()); //so the next line works
    int location = buttonGridLayout->indexOf(senderButton);
    GameLogic->stepGame(location/9,location % 9);

    int tmp = GameLogic->ValueMatrix[location / 9][location % 9];

    senderButton->setText( tmp == 0 ? "" : QString::number(tmp));
}

void Displayy::hardcoremode()
{
    GameLogic->hardcoremode();
}

void Displayy::hardcoreinterval()
{
    GameLogic->hardcoreinterval();
    btn_Hardcore_interval->setText(QString::number(GameLogic->hardcoretime_interval));
}

void Displayy::GameOver()
{
    QMessageBox::information(this, ("Game Over"),
                             ("YOU DIED"));
    startNewGame();

}


//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //
