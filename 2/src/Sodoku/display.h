#ifndef DISPLAY_H
#define DISPLAY_H

#include <QMainWindow>
#include <QMessageBox>
#include <QPushButton>
#include <QApplication>
#include <QLabel>
#include <QVector>
#include <QHBoxLayout>
#include <QString>
#include <QLCDNumber>
#include "logic.h"

class Displayy : public QWidget
{
    Q_OBJECT

private slots:
    void startNewGame();
    void saveGame();
    void loadGame();
    void pauseGame();
    void hardcoremode();
    void hardcoreinterval();
    void btnClick();
    void GameOver();




public:
    Displayy(QWidget *parent = nullptr);
    ~Displayy();

    QPushButton* btn_NewGame;
    QPushButton* btn_Pause;
    QPushButton* btn_Save;
    QPushButton* btn_Load;
    QLCDNumber*  lcd_Time;
    QPushButton* btn_Hardcore;
    QPushButton* btn_Hardcore_interval;
    QLCDNumber*  lcd_TimeHardcore;
    QGridLayout *buttonGridLayout;
    QVector<QVector<QPushButton*> > btnTable; //gamefield


    Logic* GameLogic;
};

#endif // DISPLAY_H


//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //
