#include "test.h"



void Test::initTestCase()
{
_logic = new Logic;
}

void Test::cleanupTestCase()
{
delete _logic;
}

void Test::NewGameTestCase()
{
_logic->startNewGame();
QCOMPARE(_logic->hardcoretime, 0);
QCOMPARE(_logic->_gametime, 0);
QCOMPARE(_logic->hardcoretime_interval, 20);

for (int i = 0; i < 3; i++)
for (int j = 0; j < 3; j++)
QCOMPARE(_logic->ValueMatrix[i][j],0);
}

void Test::testStepGame()
{
    //Sor
 _logic->stepGame(0,0);
 _logic->stepGame(0,8);

 QCOMPARE(_logic->ValueMatrix[0][0],1);
 QCOMPARE(_logic->ValueMatrix[0][8],2);
   //oszlop
  _logic->stepGame(8,0);
   QCOMPARE(_logic->ValueMatrix[8][0],2);
   //ház
   _logic->stepGame(2,2);
   QCOMPARE(_logic->ValueMatrix[2][2],2);
}


void Test::testStepGameErrors()
{
    _logic->startNewGame();

    _logic->stepGame(-1,1);
    _logic->stepGame(0,-1);
    _logic->stepGame(9,1);
    _logic->stepGame(3,9);
    for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
    QCOMPARE(_logic->ValueMatrix[i][j],0);
}




//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //
