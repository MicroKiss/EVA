#ifndef LOGIC_H
#define LOGIC_H

#include <QTimer>
#include <QVector>
#include <QFile>
#include <QTextStream>


class Logic : public QObject
{
        Q_OBJECT

private slots:
    void timer_TimeOut();
    void timer_hardcore_TimeOut();

signals:
    void time_change(int time); // üzenetváltás eseménye
    void time_hardcore_change(int time); // üzenetváltás eseménye
    void gameover();


public slots:
    void startNewGame(); // új játék
    void pauseGame(); // játék szüneteltetése
    void stepGame(int i,int j); // lépés a játékban
    void saveGame();
    void loadGame();
    void hardcoremode();
    void hardcoreinterval();

public:
    Logic();
    int GetHouse(int i,int j);

public:

    QVector<QVector<int> > ValueMatrix;


    QTimer* _timer; // idõzítő
    QTimer* _timer_hardcore; // idõzítő
    int _gametime;
    int hardcoretime;
    int hardcoretime_interval;
    bool paused;
private:
    QString savefilename = "save.dat";
  //  QFile savefile(savefilename);
};

#endif // LOGIC_H


//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //
