#include "logic.h"

Logic::Logic()
{
    _gametime = 0;
    hardcoretime = 0;
    hardcoretime_interval = 20;
    paused = false;
    ValueMatrix.resize(9);
    for (int i = 0;i < 9;++i)
    {
        ValueMatrix[i].resize(9);

    }


    _timer = new QTimer();
    _timer_hardcore = new QTimer();
    _timer->start();
    _timer->setInterval(1000);
    _timer_hardcore->setInterval(1000);
     connect(_timer, SIGNAL(timeout()), this, SLOT(timer_TimeOut()));
     connect(_timer_hardcore, SIGNAL(timeout()), this, SLOT(timer_hardcore_TimeOut()));
}


void Logic::stepGame(int i,int j)
{
    if(i > 8 || i < 0 || j > 8 || j < 0)
        return;


    ValueMatrix[i][j] = (ValueMatrix[i][j] + 1) % 10;
    int value = ValueMatrix[i][j];
    if (value == 0) return;
    bool found = false;

       //Vertical line
        for (int a = 0; a < 9;++a)
        {
            if(ValueMatrix[a][j] == ValueMatrix[i][j] && a != i)
            {
                found = true;
                break;
             }
       }
        //Horizontal line
         for (int a = 0; a < 9;++a)
         {
             if(ValueMatrix[i][a] == ValueMatrix[i][j] && a != j)
             {
                 found =true;
                 break;
             }
        }

       //House
         int house = GetHouse(i,j);

         for (int k = 0;k <9;++k)
         {
             if(found)
                 break;
             for(int l = 0;l < 9;++l)
             {
                 if(house == GetHouse(k,l))
                 {
                     if(ValueMatrix[k][l] == ValueMatrix[i][j] && k != i && j != l)
                     {
                         found =true;
                         break;
                     }

                 }

             }

         }


        if (found) stepGame(i,j);

       hardcoretime = 0;
}

void Logic::startNewGame()
{
    hardcoretime = 0;
    _gametime = 0;
for(int i =0;i < 9;++i)
    for(int j = 0;j < 9;++j)
        ValueMatrix[i][j] = 0;
}

void Logic::pauseGame()
{
if( _timer->isActive())
    _timer->stop();
else
    _timer->start();

paused = !paused;
}

int Logic::GetHouse(int i, int j)
{
   int rows = i/3;
   int columns = j/3;

   return rows*3+columns;


}

void Logic::timer_TimeOut()
{
    time_change(++_gametime);
}

void Logic::timer_hardcore_TimeOut()
{
    time_hardcore_change(++hardcoretime);

    if(hardcoretime >= hardcoretime_interval)
        gameover();

}

void Logic::saveGame()
{
QFile file("data.dat");
if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
      return;

QTextStream out(&file);
  out << _gametime << " ";

  for (int i = 0;i < 9;++i)
  {
      for(int j = 0;j < 9;++j)
      {
         out << ValueMatrix[i][j] << " ";
      }

  }

}
void Logic::loadGame()
{
QFile file("data.dat");
if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    return;

QTextStream in(&file);
in >> _gametime;

for (int i = 0;i < 9;++i)
{
    for(int j = 0;j < 9;++j)
    {
        in >> ValueMatrix[i][j];
    }

}

}

void Logic::hardcoremode()
{
     if (_timer_hardcore->isActive())
         _timer_hardcore->stop();
     else
         _timer_hardcore->start();
}

int max(int a,int b)
{
    return (a > b ? a : b);

}

void Logic::hardcoreinterval()
{
hardcoretime_interval =(++hardcoretime_interval % 61);
hardcoretime_interval =max(hardcoretime_interval,5);

}



//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //
