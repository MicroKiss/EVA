#include "logic.h"

Logic::Logic()
{
    game_size=game_size_curr = 5;

    ValueMatrix.resize(game_size_curr);
    for(auto i: ValueMatrix)
        i.resize(game_size_curr);
}


void Logic::change_game_size()
{
    switch (game_size){
        case (5):{ game_size = 7;break;}
        case (7):{ game_size = 9;break;}
        case (9):{ game_size = 5;break;}
    };

//    qWarning() << "game size: " << game_size ;
//    qWarning() << "game size curr: " << game_size_curr ;

}


void Logic::startNewGame()
{
    score_BLUE = score_RED = 0;
    player_curr = true;

    ValueMatrix.resize(game_size);
    for(int i= 0;i <game_size ;++i)
        ValueMatrix[i].resize(game_size);
    game_size_curr = game_size;


    for(int i =0;i < game_size;++i)
        for(int j = 0;j < game_size;++j)
        {
            if(i == game_size/2 && j == i)
            {
                ValueMatrix[i][j] = 99;
            }
            else
            {
                ValueMatrix[i][j] = 0;
        }
    }

    //setting ships
    for(int i = 0;i < (game_size-1)/2;++i)
    {
        ValueMatrix[i][i] = ValueMatrix[i][game_size-i-1]= 1;


        ValueMatrix[game_size-i-1][i] = ValueMatrix[game_size-i-1][game_size-i-1]= 2;

    }



    ship_selected = false;
}


void Logic::saveGame()
{
    QVector<int> data;
        data.push_back(game_size_curr);
        data.push_back(score_BLUE);
        data.push_back(score_RED);
        data.push_back(player_curr);
        for(int i =0;i < game_size_curr;++i)
            for(int j = 0;j < game_size_curr;++j)
            {
                   data.push_back(ValueMatrix[i][j]);
            }
    Data_Acces.saveGame(data);
    qWarning() << "game saved";
}

void Logic::loadGame()
{
    QVector<int> data = Data_Acces.loadGame();
    if(data[0] == 0)
    {
        qWarning() << "no older save" ;
        return;
    }
    game_size_curr = game_size=data[0];
    ValueMatrix.resize(game_size_curr);
    for(int i= 0;i <game_size_curr ;++i)
        ValueMatrix[i].resize(game_size_curr);


    score_BLUE = data[1];
    score_RED = data[2];
    player_curr = data[3];


    for(int i =0;i < game_size_curr;++i)
        for(int j = 0;j < game_size_curr;++j)
        {
               ValueMatrix[i][j] = data[4+i*game_size_curr+j];
        }

    qWarning() << "game loaded";
}


void Logic::stepGame(int i,int j)
{



    if(i > game_size_curr-1 || i < 0 || j > game_size_curr-1 || j < 0)
        return;

    //0 empty
    //99 blackhole
    //1 blue ship
    //2 red ship

    if (ship_selected)
    {
        if(i == ship_position_y && j == ship_position_x)
        {
            ship_selected = false;
                qWarning() << i << j <<"ship: "<<ship_selected;
            return;
        }

        if(i == ship_position_y)
        {
            int distance = 0;
            int dir = (j > ship_position_x ? 1:-1);
            while(ship_position_x+dir*distance+dir*1 >=0 && ship_position_x+dir*distance+dir*1 < game_size_curr &&
                                                    ValueMatrix[i][ship_position_x+dir*distance+dir*1] == 0)
                distance += 1;

            if ((ship_position_x+dir*distance+dir*1 >=0 && ship_position_x+dir*distance+dir*1 < game_size_curr))// ha nemmegy ki a palyarol
            {
                if(ValueMatrix[i][ship_position_x+dir*distance+dir*1] == 99)
                {
                    ValueMatrix[ship_position_y][ship_position_x] = 0;

                    (player_curr ? score_BLUE++ : score_RED++);
                }
                else
                {
                     ValueMatrix[ship_position_y][ship_position_x] = 0;
                     ValueMatrix[i][ship_position_x+dir*distance] = (player_curr == true ? 1: 2);  //true is blue player 1 is blue ship
                }
            }
            else//ha kimenne a palyarol
            {
                ValueMatrix[ship_position_y][ship_position_x] = 0;
                ValueMatrix[i][ship_position_x+dir*distance] = (player_curr == true ? 1: 2);  //true is blue player 1 is blue ship
            }

            ship_selected = false;
            player_curr = !player_curr;
        } // i == ship_position_y

        if(j == ship_position_x)
        {
            int distance = 0;
            int dir = (i > ship_position_y ? 1:-1);
            while(ship_position_y+dir*distance+dir*1 >=0 && ship_position_y+dir*distance+dir*1 < game_size_curr &&
                                                    ValueMatrix[ship_position_y+dir*distance+dir*1][j] == 0)
                distance += 1;

            if ((ship_position_y+dir*distance+dir*1 >=0 && ship_position_y+dir*distance+dir*1 < game_size_curr))// ha nemmegy ki a palyarol
            {
                if(ValueMatrix[ship_position_y+dir*distance+dir*1][j] == 99)
                {
                    ValueMatrix[ship_position_y][ship_position_x] = 0;
                    (player_curr ? score_BLUE++ : score_RED++);
                }
                else
                {
                     ValueMatrix[ship_position_y][ship_position_x] = 0;
                     ValueMatrix[ship_position_y+dir*distance][j] = (player_curr == true ? 1: 2);  //true is blue player 1 is blue ship
                }
            }
            else//ha kimenne a palyarol
            {
                ValueMatrix[ship_position_y][ship_position_x] = 0;
                ValueMatrix[ship_position_y+dir*distance][j] = (player_curr == true ? 1: 2);  //true is blue player 1 is blue ship
            }

            ship_selected = false;
            player_curr = !player_curr;
        } // j == ship_position_x

    }
    else  //if ship_sleected == false
    {
        if(player_curr == true) //true is blue
        {
            if(ValueMatrix[i][j] == 1)
            {
                ship_selected = true;
                ship_position_x = j;
                ship_position_y = i;
            }
        }
        else  //player_curr == Red
        {
            if(ValueMatrix[i][j] == 2)
            {
                ship_selected = true;
                ship_position_x = j;
                ship_position_y = i;
            }
        }
    }

        qWarning() << i << j <<"ship: "<<ship_selected;
        qWarning() << "bs" << score_BLUE ;
        check_game();
}//stepgame i j


void Logic::check_game()
{
    auto max = [](auto a,auto b){return (a > b ? a: b);};

  if (max(score_BLUE,score_RED) >= (game_size_curr-1)/2)
    {

               gameover();
    }
}
