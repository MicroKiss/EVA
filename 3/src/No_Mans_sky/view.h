#ifndef VIEW_H
#define VIEW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QPushButton>
#include <QApplication>
#include <QLabel>
#include <QVector>
#include <QHBoxLayout>
#include <QString>
#include "logic.h"

//class view : public QMainWindow
//{
//    Q_OBJECT
class view : public QWidget
{
    Q_OBJECT
public:
    view(QWidget *parent = 0);
    ~view();

public slots:
    void startNewGame(); // új játék
    void change_game_size(); //
 //   void stepGame(int i,int j); // lépés a játékban
    void saveGame();
    void loadGame();
    void btnClick();
    void myUpdate();

private slots:
    void GameOver();


 private:
    QPushButton* btn_NewGame;
    QPushButton* btn_GameSize_Changer;
    QPushButton* btn_Save;
    QPushButton* btn_Load;
    QGridLayout *buttonGridLayout;
    QVector<QVector<QPushButton*> > btnTable; //gamefield
    int gamesize = 5;
    Logic *GameLogic;

};

#endif // VIEW_H



//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //
