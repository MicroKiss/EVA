#include "view.h"

view::view(QWidget *parent)
    : QWidget(parent)
{
    GameLogic = new Logic;
    setFixedSize(500,600);
    setWindowTitle("No Man's Sky");

    ///logic event
           connect(GameLogic, SIGNAL(gameover()), this, SLOT(GameOver()));


    btn_NewGame = new QPushButton("New Game");
    btn_NewGame->setFont(QFont("Times New Roman",20,QFont::Bold));
    connect(btn_NewGame, SIGNAL(clicked()), this, SLOT(startNewGame()));

    btn_GameSize_Changer = new QPushButton("5 x 5");
    btn_GameSize_Changer->setFont(QFont("Times New Roman",20,QFont::Bold));
    connect(btn_GameSize_Changer, SIGNAL(clicked()), this, SLOT(change_game_size()));
    QHBoxLayout *hLayout = new QHBoxLayout();
    hLayout->addWidget(btn_GameSize_Changer);
    hLayout->addWidget(btn_NewGame);

    // game buttons
    buttonGridLayout = new QGridLayout(); // to position the buttons in a # way

    btnTable.resize(GameLogic->game_size);
    for (int i =0; i < GameLogic->game_size;++i)
    {
        btnTable[i].resize(GameLogic->game_size);
        for (int j = 0;j < GameLogic->game_size;++j)
        {
            btnTable[i][j] = new QPushButton();
            btnTable[i][j]->setFont(QFont("Times New Roman",20,QFont::Bold));
            buttonGridLayout->addWidget(btnTable[i][j],i,j);
            connect(btnTable[i][j],SIGNAL(clicked()),this,SLOT(btnClick()));
        }
    }

         //save and load btns
     btn_Save = new QPushButton("Save");
     btn_Load = new QPushButton("Load");
     connect(btn_Save, SIGNAL(clicked()), this, SLOT(saveGame()));
     connect(btn_Load, SIGNAL(clicked()), this, SLOT(loadGame()));
     btn_Save->setFont(QFont("Times New Roman",20,QFont::Bold));
     btn_Load->setFont(QFont("Times New Roman",20,QFont::Bold));



    QHBoxLayout *hLayout2 = new QHBoxLayout();
    hLayout2->addWidget(btn_Save);
    hLayout2->addWidget(btn_Load);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(hLayout);
    mainLayout->addLayout(buttonGridLayout);
    mainLayout->addLayout(hLayout2);

    setLayout(mainLayout);


startNewGame();
}

view::~view()
{
    for (auto i : btnTable)
        for(auto j : i)
            delete j;
}


void view::startNewGame()
{

    GameLogic->startNewGame();

//    destroying the table
       for(auto i : btnTable)
           for(auto j : i)
               delete j;

    btnTable.resize(GameLogic->game_size);
    for (int i =0; i < GameLogic->game_size;++i)
    {
        btnTable[i].resize(GameLogic->game_size);
        for (int j = 0;j < GameLogic->game_size;++j)
        {
            btnTable[i][j] = new QPushButton();
            btnTable[i][j]->setFont(QFont("Times New Roman",20,QFont::Bold));
            btnTable[i][j]->setText(QString::number(GameLogic->ValueMatrix[i][j]));
            buttonGridLayout->addWidget(btnTable[i][j],i,j);
            connect(btnTable[i][j],SIGNAL(clicked()),this,SLOT(btnClick()));
        }
    }
    myUpdate();
}//startNewGame

void view::change_game_size()
{
    GameLogic->change_game_size();

    btn_GameSize_Changer->setText(QString::number(GameLogic->game_size) + QString(" x ") + QString::number(GameLogic->game_size));

}

void view::saveGame()
{
    GameLogic->saveGame();

}

void view::loadGame()
{
    GameLogic->loadGame();
    //    destroying the table
   for(auto i : btnTable)
       for(auto j : i)
           delete j;
    btn_GameSize_Changer->setText(QString::number(GameLogic->game_size) + QString(" x ") + QString::number(GameLogic->game_size));
   btnTable.resize(GameLogic->game_size);
   for (int i =0; i < GameLogic->game_size;++i)
   {
       btnTable[i].resize(GameLogic->game_size);
       for (int j = 0;j < GameLogic->game_size;++j)
       {
           btnTable[i][j] = new QPushButton();
           btnTable[i][j]->setFont(QFont("Times New Roman",20,QFont::Bold));
           btnTable[i][j]->setText(QString::number(GameLogic->ValueMatrix[i][j]));
           buttonGridLayout->addWidget(btnTable[i][j],i,j);
           connect(btnTable[i][j],SIGNAL(clicked()),this,SLOT(btnClick()));
       }
   }


       myUpdate();
}

void view::btnClick()
{

    QPushButton* senderButton = dynamic_cast<QPushButton*>(QObject::sender()); //so the next line works
    int location = buttonGridLayout->indexOf(senderButton);
    GameLogic->stepGame(location/GameLogic->game_size,location % GameLogic->game_size);

    for (int i =0; i < GameLogic->game_size;++i)
    {
        for (int j = 0;j < GameLogic->game_size;++j)
        {
            btnTable[i][j]->setText(QString::number(GameLogic->ValueMatrix[i][j]));
        }
    }


    myUpdate();
}



void view::GameOver()
{

        QString tmp =GameLogic->score_BLUE > GameLogic->score_RED ? "BLUE" : "RED";
              tmp = tmp + " has won the game!!!4";
        QMessageBox::information(this,trUtf8("No Man'S Sky"), tmp);
        startNewGame();

}


void::view::myUpdate()
{


    for (int i =0; i < GameLogic->game_size;++i)
    {
        for (int j = 0;j < GameLogic->game_size;++j)
        {
            switch(GameLogic->ValueMatrix[i][j]) {
                case 1 :
                   btnTable[i][j]->setStyleSheet("background-color:blue;");break;
                case 2 :
                   btnTable[i][j]->setStyleSheet("background-color:red;");break;
                case 99 :
                    btnTable[i][j]->setStyleSheet("background-color:black;");break;
                case 0 :
                btnTable[i][j]->setStyleSheet("background-color:white;");break;
            }
        }
    }



}
