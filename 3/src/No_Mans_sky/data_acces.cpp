#include "data_acces.h"




void data_acces::saveGame(QVector<int> data)
{
QFile file(savefilename);
if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
      return;

QTextStream out(&file);

for(int i = 0;i < data.size();++i)
    out << data[i] << " ";

}

QVector<int> data_acces::loadGame()
{
QFile file(savefilename);
if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    return (QVector<int>(0));

QTextStream in(&file);
QVector<int> data;
int tmp;
while(!in.atEnd())
{
    in >> tmp;
    data.push_back(tmp);
}
return data;
}
