#include "test.h"



void Test::initTestCase()
{
_logic = new Logic;
}

void Test::cleanupTestCase()
{
delete _logic;
}

void Test::NewGameTestCase()
{
_logic->startNewGame();
QCOMPARE(_logic->score_BLUE, 0);
QCOMPARE(_logic->score_RED, 0);
QCOMPARE(_logic->game_size, 5);
QCOMPARE(_logic->game_size_curr, 5);
QCOMPARE(_logic->ship_selected,false);

QCOMPARE(_logic->ValueMatrix[_logic->game_size_curr/2][_logic->game_size_curr/2],99);
}

void Test::testStepGame()
{
    //Sor es hajoutkozes
 _logic->stepGame(0,0);


 QCOMPARE(_logic->ship_selected,true);

 _logic->stepGame(0,1);

 QCOMPARE(_logic->ship_selected,false);

 QCOMPARE(_logic->ValueMatrix[0][0],0);
 QCOMPARE(_logic->ValueMatrix[0][_logic->game_size_curr-2],1);
 QCOMPARE(_logic->ValueMatrix[0][_logic->game_size_curr-1],1);
   //oszlop es falutkozes
  _logic->stepGame(_logic->game_size_curr-1,0);

  QCOMPARE(_logic->ship_selected,true);

  _logic->stepGame(_logic->game_size_curr-2,0);

  QCOMPARE(_logic->ship_selected,false);

  QCOMPARE(_logic->ValueMatrix[_logic->game_size_curr-1][0],0);
   QCOMPARE(_logic->ValueMatrix[0][0],2);

   _logic->change_game_size();

     QCOMPARE(_logic->game_size,7);
     QCOMPARE(_logic->game_size_curr,5);
}


void Test::testStepGameErrors()
{
    _logic->startNewGame();

    _logic->stepGame(-1,1);
    _logic->stepGame(0,-1);
    _logic->stepGame(_logic->game_size_curr,0);
    _logic->stepGame(0,_logic->game_size_curr);
      QCOMPARE(_logic->ship_selected,false);
      QCOMPARE(_logic->score_BLUE, 0);
      QCOMPARE(_logic->score_RED, 0);

    for(int i = 0;i < (_logic->game_size_curr-1)/2;++i)
    {
        QCOMPARE(_logic->ValueMatrix[i][i],1);
        QCOMPARE(_logic->ValueMatrix[i][_logic->game_size_curr-i-1],1);
        QCOMPARE(_logic->ValueMatrix[_logic->game_size_curr-i-1][i],2);
        QCOMPARE(_logic->ValueMatrix[_logic->game_size_curr-i-1][_logic->game_size-i-1],2);

    }
}




//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //
