#ifndef TEST_H
#define TEST_H


#include <QtTest>
#include "logic.h"
class Test : public QObject
{
    Q_OBJECT
private:
    Logic* _logic;
private slots:
    void initTestCase();
    void cleanupTestCase();
    void NewGameTestCase();
    void testStepGame();
    void testStepGameErrors();
};


#endif // TEST_H


