#ifndef LOGIC_H
#define LOGIC_H

#include <QVector>
#include <QFile>
#include <QTextStream>
#include <QtDebug>

class Logic : public QObject
{
        Q_OBJECT

//private slots:
//    void timer_TimeOut();
 //   void timer_hardcore_TimeOut();
signals:
    void gameover();


public slots:
    void startNewGame(); // új játék
    void change_game_size(); // change gamesize
    void stepGame(int i,int j); // lépés a játékban
    void check_game();

public:
    Logic();

public:
    int game_size = 5;
    int game_size_curr = 5;
    QVector< QVector <int> > ValueMatrix;


    bool ship_selected = false; //for the step game
private:

    bool player_curr = true; //true is blue
public:
    int score_RED  =0;
    int score_BLUE  =0;
    int ship_position_x;
    int ship_position_y;
};

#endif // LOGIC_H


//	__  ___                  _   __           //
//	|  \/  (_)              | | / (_)         //
//	| .  .  _  ___ _ __ ___ | |/ / _ ___ ___  //
//	| |\/| | |/ __| '__/ _ \|    \| / __/ __| //
//	| |  | | | (__| | | (_) | |\  | \__ \__ \ //
//	\_|  |_|_|\___|_|  \___/\_| \_|_|___|___/ //
